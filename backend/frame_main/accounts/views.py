from rest_framework.generics import (ListCreateAPIView,RetrieveUpdateDestroyAPIView,)
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from .models import userProfile
from .permissions import IsOwnerProfileOrReadOnly
from .serializers import userProfileSerializer
from django.http import HttpResponse
from rest_framework import request
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt

import sys
import subprocess
# from django.http import request
# from matching_engine.helloworld import say_hello

# Create your views here.

class UserProfileListCreateView(ListCreateAPIView):
    queryset=userProfile.objects.all()
    serializer_class=userProfileSerializer
    permission_classes=[IsAuthenticated]

    def perform_create(self, serializer):
        user=self.request.user
        serializer.save(user=user)


class userProfileDetailView(RetrieveUpdateDestroyAPIView):
    queryset=userProfile.objects.all()
    serializer_class=userProfileSerializer
    # permission_classes=[IsOwnerProfileOrReadOnly,IsAuthenticated]
    permission_classes=[IsAuthenticated]



