from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class userProfile(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE,related_name="profile")
    description=models.TextField(blank=True,null=True)
    location=models.CharField(max_length=30,blank=True)
    date_joined=models.DateTimeField(auto_now_add=True)
    updated_on=models.DateTimeField(auto_now=True)
    is_organizer=models.BooleanField(default=False)
    user_images_taken=models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

# {
#     "id": 1,
#     "user": "rohan",
#     "description": "baburao123",
#     "location": "mumbai",
#     "date_joined": "2020-01-11T06:48:18.226245Z",
#     "updated_on": "2020-01-11T06:49:23.016602Z",
#     "is_organizer": false,
#     "user_images_taken": false
# }